<?
/**
 * This is a CodeIgniter controller that handles user functions
 * for managing listings. 
 */
class My_account extends CI_Controller {
    
    function __construct() {
        parent::__construct();        
        //$this->output->enable_profiler(TRUE);
        
        // Must be logged in for all my_account functionality
        if( !$this->tank_auth->is_logged_in()) redirect('/auth/login');
    }
    
    /**
     * Loads the "welcome" view
     */
    function index() {
        $this->load->view($GLOBALS['dcfg']->theme.'/header', array('title'=>'My Account'));
        $this->load->view($GLOBALS['dcfg']->theme.'/main/my-account');
        $this->load->view($GLOBALS['dcfg']->theme.'/sidebar/member');
        $this->load->view($GLOBALS['dcfg']->theme.'/footer');             
    }

    /**
     * Checks to see if the listing is owned by the logged in user. If not, displays 404 error.
     * Returns the location object on success.
     * 
     * @param int
     * @return object
     */
     function _verify_ownership($location_id) {
        $location = $this->Location_model->get_location_by_id($location_id);
        if(isset($location->user_id) && $location->user_id == $this->tank_auth->get_user_id()) {
            return $location;
        } else {
            show_404("my_account/_verify_ownership/$location_id");
        }
     }

    /**
     * ================= Create listing helpers ======================
     * These represent the three parts of the intial listing creating process.
     */

    /**
     * Displays the create listing form.
     */
    function _show_new_listing_form() {
        $this->load->model('Geography_model');
        $data['states'] = $this->Geography_model->get_states();

        $this->load->view($GLOBALS['dcfg']->theme.'/header', array('title'=>'Create a Listing'));
        $this->load->view($GLOBALS['dcfg']->theme.'/account/create_listing', $data);
        $this->load->view($GLOBALS['dcfg']->theme.'/sidebar/member');
        $this->load->view($GLOBALS['dcfg']->theme.'/footer');
    }

    /**
     * Displays a page of duplicate listings, allowing the user to take over one 
     */
    function _show_duplicates($duplicates, $fields) {
        $data['duplicates'] = $duplicates;
        $data['fields'] = $fields; // The original form fields for use if they continue with creation

        $this->load->view($GLOBALS['dcfg']->theme.'/header', array('title'=>'Are any of these your listing?'));
        $this->load->view($GLOBALS['dcfg']->theme.'/account/create_listing_show_duplicates', $data);
        $this->load->view($GLOBALS['dcfg']->theme.'/sidebar/member');
        $this->load->view($GLOBALS['dcfg']->theme.'/footer');
    }

    /** 
     * Creates the listing and redirects to the new listing page
     */
    function _save_listing($fields) {
        $new_id = $this->Location_model->add_location($fields);

        if($new_id == 0) {
            $this->messages->add('error', 'Failed to create listing.');
            log_message('error', 'Failed to create listing.');
        } else {
            $geo_status = $this->Location_model->update_geocode_data($new_id);
            $this->messages->add('success', "'".htmlspecialchars($fields['name'])."' has been created! You can now add more details and photos!");
            redirect('/my_account/view/'.$new_id);
        }
    }

    /**
     * Creates a new unapproved listing for the logged in user.
     * 
     * Checks for listings with similar names within 15miles and suggests them 
     * 
     * Now broken into these steps
     *  1. enter essential listing details
     *  2. check for similarly named listings within 15miles and offer to claim them
     *  3. proceed to claim form, or continue creating listing
     *  2. use the name and state_id to look for duplicates
     * 
     * @return void
     */
    function create($next=false) {
        $this->load->model('Geography_model');
        $this->load->library('form_validation');

        if(!$_POST) {
            $this->_show_new_listing_form();

        } else {
            $fields = array('name' =>           $this->input->post('name'),
                            'address' =>        $this->input->post('address'),
                            'city' =>           $this->input->post('city'),
                            'state_id'=>        $this->input->post('state_id'),
                            'postal_code'=>     $this->input->post('postal_code'),
                            'user_id'=>         $this->tank_auth->get_user_id(),
                        );

            // Validate data
            $this->form_validation->set_rules('name',   'Listing Name',         'trim|required');
            $this->form_validation->set_rules('address', 'Address',             'trim|required');
            $this->form_validation->set_rules('city', 'City',                   'trim|required');
            $this->form_validation->set_rules('state_id', 'State/Province',     'trim|required');
            $this->form_validation->set_rules('postal_code', 'Zip/Postal Code', 'trim|required');

            if($this->form_validation->run() === FALSE) {
                $this->messages->add('error', 'All fields are required.'); 
                $this->_show_new_listing_form();

            } else {
                if($this->input->post('skip_duplicate_check')) {
                    $this->_save_listing($fields);    

                } else {
                    $duplicates = $this->Location_model->find_duplicate_listings($fields['name'], $fields['address'], $fields['city'], $fields['state_id'], $fields['postal_code']);

                    if($duplicates) {
                        $this->_show_duplicates($duplicates, $fields);

                    } else {
                        $this->_save_listing($fields);
                    }
                }
            }
        }            
    }
     
     
    /**
     * Deletes a location
     * 
     */
    function delete() {
        $location_id = FALSE;
        if($_POST) {
            $location_id = $this->input->post('location_id');
            $location = $this->_verify_ownership($location_id);
            
            $this->load->model('Image_model');
            $this->load->model('Extra_field_model');
            
            if($this->Location_model->delete_location($location_id)) {
                // Delete images
                $images = $this->Image_model->get_images($location_id);
                foreach($images as $image) $this->Image_model->delete_image($image->id);
                
                // Delete extra field values
                $this->Extra_field_model->delete_values($location_id);
                
                $this->messages->add('success', 'Listing deleted.');
                redirect('/my_account');
            } else {
                $this->messages->add('error', 'Failed to delete listing.');
            }
        }    
        
        if($location_id) {
            redirect('/my_account/view/'.$location_id);
        } else {
            redirect('/my_account');
        }
    }

    
    /**
     * Update the standard fields for a listing. Acts on the locations table.
     * 
     * @param int
     */
    function _update($location_id) {
        $location = $this->_verify_ownership($location_id);
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('name',   'Listing Name',         'trim|required');
        $this->form_validation->set_rules('address', 'Address',             'trim|required');
        $this->form_validation->set_rules('city', 'City',                   'trim|required');
        $this->form_validation->set_rules('state_id', 'State/Province',     'trim|required');
        $this->form_validation->set_rules('postal_code', 'Zip/Postal Code', 'trim|required');
        $this->form_validation->set_rules('website', 'Website',             'trim|prep_url');
        $this->form_validation->set_rules('email', 'Email',                 'trim|valid_email');
        $this->form_validation->set_rules('phone', 'Phone',                 'trim');
        $this->form_validation->set_rules('contact_name', 'Contact Name',   'trim');
        $this->form_validation->set_rules('description', 'Description',     'trim');
        $this->form_validation->set_rules('hours', 'Hours',                 'trim');
        
        if($this->form_validation->run() === FALSE) {
            // Handle errors
            $this->messages->add('error', validation_errors(' ', '<br>'));

        } else {            
            // Success
            $updates = array(   'name' =>           $this->input->post('name'),
                                'address' =>        $this->input->post('address'),
                                'city' =>           $this->input->post('city'),
                                'state_id'=>        $this->input->post('state_id'),
                                'postal_code'=>     $this->input->post('postal_code'),
                                'website'=>         $this->input->post('website'),
                                'email'=>           $this->input->post('email'),
                                'phone'=>           $this->input->post('phone'),
                                'contact_name'=>    $this->input->post('contact_name'),
                                'description'=>     $this->input->post('description'),
                                'hours'=>           $this->input->post('hours')            
                            );            
            $rows_affected = $this->Location_model->update($location_id, $updates);
            
            if($rows_affected === 1) {
                $this->messages->add('success', 'Listing saved.');
                
                // Geocode
                $do_geo = FALSE;
                foreach(array('address', 'city', 'state_id', 'postal_code') as $field) {
                    if($location->$field != $this->input->post($field)) {
                        $do_geo = TRUE;
                        break;
                    }
                }                    
                if($do_geo) {
                    $geo_status = $this->Location_model->update_geocode_data($location_id);
                    $this->messages->add('message', "You changed your address so your map marker may have moved.");
                }
                
                // If they change their TLD and they had ads removed via a linkback
                // they have to re-verfify the linkback.
                if($location->verified_linkback == 1) {
                    $location_website_host = parse_url(prep_url($location->website));
                    $location_website_host = preg_replace('/www\./', '', strtolower($location_website_host['host']));
                    $input_website_host = parse_url(prep_url($this->input->post('website')));
                    $input_website_host = preg_replace('/www\./', '', strtolower($input_website_host['host']));
                    
                    if($input_website_host != $location_website_host) {
                        $this->Location_model->update($location_id, array('verified_linkback'=> 0));
                        $this->messages->add('message', "You changed your website so ads have been enabled for your listing. You can have them removed again by re-verifying your link to us under the 'Remove Ads' tab.");
                    }
                }
                
            } elseif(is_null($rows_affected)) {
                $this->messages->add('error', 'Failed to update listing.');
            }
        }
    }

    /**
     * Confirms the existence of a link on a webpage for this location with CURL
     * AND that the page is part of the TLD that matches the listings website.
     * 
     * @param int
     */
    function verify_linkback($location_id=0) {
        $location = $this->_verify_ownership($location_id);
        $this->load->library('form_validation');
        
        if($_POST) {
            $this->form_validation->set_rules('webpage', 'Web Page', 'trim');
            
            if($this->form_validation->run() === FALSE) {
                $this->messages->add('error', validation_errors(' ', "<br>"));
                
            } else {            
                // Confirm that the url they provide is part of the same TLD as the location's website
                $location_website_parts = parse_url(prep_url($location->website));
                if( !isset($location_website_parts['host'])) {
                    $this->messages->add('error', 'Please make sure the website for your listing is real. You can change this in the \'General\' tab.');
                    
                } else {
                    $location_host = preg_replace('/www\./', '', strtolower($location_website_parts['host']));
                    
                    $prepped_url = prep_url($this->input->post('webpage'));
                    $input_website_parts = parse_url($prepped_url);
                    if( !isset($input_website_parts['host'])) {
                        $this->messages->add('error', 'That doesn\'t appear to be a real web page.');
                        
                    } else {
                        $input_host = preg_replace('/www\./', '', strtolower($input_website_parts['host']));
                        
                        if($location_host != $input_host) {
                            $this->messages->add('error', 'The domain on your listing ('.htmlspecialchars($location_host).') must match the domain with the link ('.htmlspecialchars($input_host).').');
                            
                        } else {
                            // Let's see for ourselves so they can't trick us.
                           $ch=curl_init();
                           curl_setopt ($ch, CURLOPT_URL, $prepped_url);
                           curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
                           curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
                           curl_setopt ($ch,CURLOPT_VERBOSE, false);
                           curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                           curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, FALSE);
                           curl_setopt($ch,CURLOPT_SSLVERSION,3);
                           curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, FALSE);
                           curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
                           curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
                           $html=curl_exec($ch);
                           $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                           curl_close($ch);
                           if($httpcode<200 || $httpcode>=300) {
                                $this->messages->add('error', "We couldn't load ".htmlspecialchars($prepped_url)." (HTTP Error: {$httpcode})");
                                
                            } else {
                                // If our link is there we will update their listing.
                                if(preg_match('/<a.*?href=".*?'.preg_replace('/\./', '\.', $GLOBALS['dcfg']->domain).'.*?">.*?<\/a>/i', $html) == 0) {
                                    $this->messages->add('error', "We couldn't find a link to ".$GLOBALS['dcfg']->domain.' on '.htmlspecialchars($prepped_url));
                                } else {
                                    $changes = array('verified_linkback'=> 1);
                                    if($this->Location_model->update($location->id, $changes) == 1) {
                                        $this->messages->add('success', 'The ads have been removed from your listing!');
                                        
                                        // Notify me
                                        $this->load->library('email');
                                        $this->email->from($GLOBALS['dcfg']->email);
                                        $this->email->to($GLOBALS['dcfg']->email);
                                        $this->email->subject('New Linkback!');                                        
                                        $message = "<h2>New linkback</h2>";
                                        $message .= '<p><a href="'.htmlspecialchars($prepped_url).'">'.htmlspecialchars($prepped_url).'</a></p>';
                                        $this->email->message($message);
                                        $this->email->send();
                                        
                                    } else {
                                        $this->messages->add('error', 'Failed to remove ads from listing.');
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        redirect('/my_account/view/'.$location_id);
    }

    
    /**
     * Updates extra fields for a listing. Acts on the extras tables.
     * 
     * @param int
     */
    function update_extras($location_id=0) {
        $this->_verify_ownership($location_id);
        
        $this->load->library('form_validation');
        $this->load->model('Extra_field_model');
        
        if($_POST) {
            $extra_fields = $this->Extra_field_model->get_fields();
            foreach($extra_fields as $field) {
                $this->form_validation->set_rules('extra_'.$field->id, $field->field, 'trim');
            }
            
            if($this->form_validation->run() === FALSE) {
                log_message('error', 'update_extras() form validation failed.');
                
            } else {            
                foreach($extra_fields as $field) {
                    $this->Extra_field_model->update($field->id, $location_id, $this->input->post('extra_'.$field->id));
                }
                
                $this->messages->add('success', 'Listing saved.');
            }
        } 
        
        redirect('/my_account/view/'.$location_id);
    }
    
    
    /**
     * Manually overrides the long/lat in our locations database.
     * 
     * @param id
     * @return bool
     * 
     */
    function update_coordinates($location_id) {
        $location = $this->_verify_ownership($location_id);
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('lat', 'Latitude', 'trim|required|numeric');
        $this->form_validation->set_rules('lng', 'Longitude', 'trim|required|numeric');
        
        if($this->form_validation->run() === FALSE) {
            $this->messages->add('error', validation_errors(' ', "<br>"));
            
        } else {
            $result = $this->Location_model->update_coordinates($location_id, floatval($this->input->post('lat')), floatval($this->input->post('lng')));
        
            if($result == TRUE) {
                $this->messages->add('success', 'Marker saved!');
                $this->messages->add('message', 'Note: If you now decide to change your address the map marker will be reset.');
            } else {
                $this->messages->add('error', 'Failed to update marker.');
            }
        }
        
        redirect('/my_account/view/'.$location_id);
    }
    
    /**
     * Display the control panel for editing a listing.
     * 
     */
    function view($location_id) {
        if($this->input->post('action') == 'update') {
            $this->_update($location_id);
        }
        
        $this->_load_editor($location_id);
    }

    /**
     * Displays the editor for the location
     * @param int
    */
    function _load_editor($location_id) {
        $data['location'] = $this->_verify_ownership($location_id);
        $this->load->model(array('Geography_model', 'Extra_field_model', 'Image_model'));
        
        if($data['location']->approved != 1) $this->messages->add('warning', "We need to take a look at your listing before it gets published; this usually takes a day or two. In the mean time feel free to make changes.");
        if($data['location']->geocode_status != '200') $this->messages->add('warning', "We had trouble locating you on the map. Please double check your address or move the map marker to the proper location using the 'Map Marker' tab.");
        
        // If the coords don't exist, set the map to a default location so they can correct it.
        if($data['location']->longitude == '' || $data['location']->latitude == '') {
            $f_data['zoom'] = 3;
            $data['location']->latitude = '44';
            $data['location']->longitude = '-100';
        } else {
            $f_data['zoom'] = 14;
        }
        $f_data['latitude'] = $data['location']->latitude;
        $f_data['longitude'] = $data['location']->longitude;        
        $f_data['google_map'] = TRUE;
        $f_data['marker_draggable'] = TRUE;
        
        // States and countries drop downs
        $data['countries'] = $this->Geography_model->get_countries();
        $data['states'] = $this->Geography_model->get_states();
        $data['fields'] = $this->Extra_field_model->get_fields_and_values($location_id);
        
        // Images
        $data['images'] = $this->Image_model->get_images($location_id);
    
        $this->load->view($GLOBALS['dcfg']->theme.'/header', array('title'=>'Edit Listing'));
        $this->load->view($GLOBALS['dcfg']->theme.'/account/view', $data);
        $this->load->view($GLOBALS['dcfg']->theme.'/sidebar/member');
        $this->load->view($GLOBALS['dcfg']->theme.'/footer', $f_data); 
    }
    
    /**
     * Delete the physical image and thumbnail and removes the entry from the database.
     * 
     * @param id
     * @return void
     */
    function delete_image($image_id) {
        $this->load->model('Image_model');
        
        $image = $this->Image_model->get_image($image_id);
        if( !$image) show_404("my_account/delete_image/$image_id");
        $this->_verify_ownership($image->location_id);
        
        if($this->Image_model->delete_image($image_id)) {
            $this->messages->add('success', 'Image deleted.');
        } else {
            $this->messages->add('failure', 'Failed to delete image.');
        }
        
        redirect('/my_account/view/'.$image->location_id);
    }
    
    /**
     * Handles a post to upload an image.
     * 
     * @param int
     */
    function upload_image($location_id=0) {
        // Verify ownership
        $location = $this->_verify_ownership($location_id);
        if( !$_FILES) redirect('/my_account/view/'.$location_id);
        if($location_id < 1) redirect('/my_account');
        
        $this->load->library('Image_editor');
        $this->load->model('Image_model');
        
        // Image limit
        $images = $this->Image_model->get_images($location_id);
        if(count($images) >= IMAGES_PER_LOCATION) {
            $this->messages->add('error', 'Sorry, you already have '.IMAGES_PER_LOCATION.' photos. Please delete one first.');
            redirect('/my_account/view/'.$location_id);
        }

        $config['allowed_types'] = IMAGES_TYPES;
        $config['max_size'] = IMAGES_MAX_SIZE + 10; // 10kb padding
        $config['max_width']  = IMAGES_MAX_WIDTH;
        $config['max_height']  = IMAGES_MAX_HEIGHT;
        $config['upload_path'] = $GLOBALS['dcfg']->base_path.'uploads/images/'.$location_id.'/';
        $config['file_name']  = substr($location->ref_name, 0, 20).rand(1,9999);
        $this->load->library('upload', $config);
        
        // Make sure the directory exists
        if( !is_dir($config['upload_path'])) {
            try {
                mkdir($config['upload_path'], 0777, TRUE);
                chmod($config['upload_path'], 0777);
            } catch (Exception $e) {
                // $e->getMessage()
                $this->messages->add('error', 'There was an error preparing for your upload.');
                log_message('error', 'Unable to create or chmod upload dir. '.$e);
                redirect('/my_account/view/'.$location_id);
            }
        }

        if ( !$this->upload->do_upload()) {
            $this->messages->add('error', $this->upload->display_errors(' ', "<br>"));
            redirect('/my_account/view/'.$location_id);
            
        } else {
            // Success!
            $upload_data = $this->upload->data();
            
            // Create the thumbnail
            $i = new Image_editor();
            $i->set_image($upload_data['full_path']);
            $i->create_thumb();
            $path_info = pathinfo($upload_data['full_path']);
            $thumb_name = $path_info['filename'].'_thumb.'.$path_info['extension'];
            $thumb_path = $path_info['dirname'].'/'.$thumb_name;
            $i->save_image($thumb_path);
            
            // chmod 755 both images
            chmod($upload_data['full_path'], 0755);
            chmod($thumb_path, 0755);
            
            if($this->Image_model->add_image($location_id, $upload_data['file_name'], $thumb_name)) {
                $this->messages->add('success', 'Image uploaded!');
                    
            } else {
                $this->messages->add('error', 'Unable to upload image.');
                if( !$full_result) $this->log_message('error', 'Unable to add full image to the database.');
                if( !$thumb_result) $this->log_message('error', 'Unable to add thumb image to the database.');
                
                // Delete the file in the future?
            }
                
            redirect('/my_account/view/'.$location_id);
        }
    }

}

?>
