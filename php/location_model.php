<?
/**
 * Locations
 * 
 * This model represents location data. It operates on the following tables:
 * - locations
 */
class Location_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }     
    
    /** count_locations()
     * 
     * @return int
     */
    function count_locations() {
        return $this->db->count_all('locations');
    }

    /**
    * Looks for locations similar locations on the criteria:
    * - has no owner
    * - ref_name is a substring of an existing ref_name
    * - within 30 miles
    *
    * @param str
    * @return array or NULL
    */
    function find_duplicate_listings($name, $address, $city, $state_id, $postal_code, $distance=20) {
        $ref_name = $this->name_to_ref($name);
        if( !$ref_name) return NULL;

        // Determine the country and state name from the state ID
        $state = array('state_abr' => '', 'country' => '');
        $this->db->where('state_id', $state_id);
        $this->db->join('countries', 'countries.country_id = states.country_id', 'left');
        $this->db->limit(1);
        $s_query = $this->db->get('states');
        if($s_query->num_rows() == 1) $state = $s_query->row_array();

        // Geocode
        $this->load->library('Google_map');
        $coords = $this->google_map->get_coords(sprintf('%s, %s, %s, %s', $address, $city, $state['state_abr'], $state['country']));
        if($coords) {
            $long = $coords['longitude'];
            $lat = $coords['latitude'];
        
            // Do a search
            $sql = "SELECT *, 
                    (((acos(sin((".$lat."*pi()/180)) * sin((`latitude`*pi()/180))+cos((".$lat."*pi()/180)) * cos((`latitude`*pi()/180)) * cos(((".$long."- `longitude`)*pi()/180))))*180/pi())*60*1.1515*1.609344)
                    AS distance
                    FROM locations
                        LEFT JOIN states ON states.state_id=locations.state_id 
                    WHERE 
                        geocode_status='200' AND 
                        approved=1 AND 
                        date_deleted IS NULL AND
                        (user_id = 0 OR user_id IS NULL) AND
                        ref_name LIKE '%".$ref_name."%'
                    HAVING distance<='".$distance."' 
                    ORDER BY name ASC";

            $query = $this->db->query($sql);
            if($query->num_rows() > 0) return $query->result();
        }
    }

    /**
     * Returns the last $x new listings giving preference to those with images.
     *
     */
    function get_latest_listings($limit=4) {
        $this->db->select('*, locations.id as location_id');
        $this->db->where('date_deleted is NULL', NULL, FALSE);
        $this->db->where('approved', 1);
        $this->db->where('user_id >', 0);
        $this->db->where('geocode_status', '200');
        $this->db->join('images',  'images.location_id=locations.id'); // Full join so only if images match
        $this->db->join('states', 'states.state_id=locations.state_id', 'left');
        $this->db->order_by('created', 'desc');
        $this->db->limit($limit);
        $this->db->group_by('locations.id');

        $query = $this->db->get('locations');  
        if($query->num_rows() > 0) return $query->result();
            
        // If not, try another query without images are OK this time
        $this->db->select('*, locations.id as location_id');
        $this->db->where('date_deleted is NULL', NULL, FALSE);
        $this->db->where('approved', 1);
        $this->db->where('geocode_status', '200');
        $this->db->join('states', 'states.state_id=locations.state_id', 'left');
        $this->db->order_by('RAND()');
        $this->db->limit($limit);

        $query = $this->db->get('locations');  
        return $query->result();
    }

    /**
     * Manually sets the coords for a location
     * 
     * @param int
     * @param string
     * @param string
     * @return bool
     */
     function update_coordinates($location_id, $lat, $lng) {
        $changes = array(   'latitude'=>$lat,
                            'longitude'=>$lng,
                            'geocode_status'=>'200'
                        );
        $this->db->where('id', $location_id);
        $this->db->limit(1);
        
        $this->db->set('last_update', 'NOW()', FALSE);
        $query = $this->db->update('locations', $changes);
        if($this->db->affected_rows() == 1) return TRUE;
        
        log_message('error', "Failed to update marker for location #{$location_id}. lat:{$lat}, lng:{$lng}");
        return FALSE;
     }
    
    /**
     * Updates a location
     * 
     * @param int
     * @param array - array keys match field names
     * @return int
     */
    function update($id, $data) {
        $this->db->limit(1);
        $this->db->where(array( 'id'=>              $id,
                                'user_id' =>        $this->session->userdata('user_id'),
                        ));

        $this->db->set('last_update', 'NOW()', FALSE);
        $this->db->update('locations', $data);
        if($this->db->affected_rows() > 1) {
            log_message('error', 'Failed to update location #'.$id);
            return NULL;
        } else {
            return $this->db->affected_rows();
        }
    }
    
    /**
     * Get a users locations
     *
     * @param   int
     * @return  array[object]
     */
    function get_users_locations($user_id) {
        $this->db->where('user_id', $user_id);
        $this->db->where('date_deleted is NULL', NULL, FALSE);
        
        $query = $this->db->get('locations');  
        return $query->result();
    }
    
    /** 
     * Geocodes a location based on the current address and updates the database with the geocode information
     *
     * @param int
     * @return string
     */
    function update_geocode_data($location_id) {
        $location = $this->get_location_by_id($location_id);
        if( !$location) return NULL;
        
        // Geocode
        $this->load->library('Google_map');
        $coords = $this->google_map->get_coords(sprintf('%s, %s, %s, %s', $location->address, $location->city, $location->state_abr, $location->country));
        
        $changes = array(   'longitude'=>'',
                            'latitude'=>'',
                            'geocode_status' => 'ZERO_RESULS' );
        if($coords) {
            $changes['longitude'] = $coords['longitude'];
            $changes['latitude'] = $coords['latitude'];
            $changes['geocode_status'] = $coords['status'];
        }
        $this->db->where('id', $location_id);
        $this->db->limit(1);
        $this->db->set('geocode_date', 'NOW()', FALSE);
        
        $this->db->update('locations', $changes);
        return $changes['geocode_status'];
    }


    /**
     * Approves a location
     *
     * @param id
     * @return int
     */
    function approve($location_id) {
        $this->db->set('approved', 1);
        $this->db->where('id', $location_id);
        $this->db->limit(1);

        $this->db->update('locations');
        return $this->db->affected_rows();
    }
    
    /**
     * Updates the user_id (owner) of a listing
     * 
     * @param int
     * @param int
     * @return int
     */
    function set_owner($location_id, $user_id) {
        $this->db->set('user_id', $user_id);
        $this->db->where('id', $location_id);
        $this->db->limit(1);
        
        $this->db->update('locations');
        return $this->db->affected_rows();
    }

    /**
     * Creates a new listing and returns its ID. Also creates a ref_name.
     * This emails the site owner if it's on production
     * 
     * @param array
     * @return int
     */
    function add_location($fields) {
        $fields['ref_name'] = $this->make_ref_name($fields);
        if($fields['ref_name'] == '') {
            log_message('error', 'Failed to create a new listing. Could not generate a ref_name!');
            return 0;
        }
        $this->db->set('created', 'NOW()', FALSE);
        
        $this->db->insert('locations', $fields);
        if($this->db->affected_rows() == 1) {
            $new_id = $this->db->insert_id();
            
            // Let me know!
            if($GLOBALS['dcfg']->environment == 'production') {
                $this->load->library('email');
                $this->email->from($this->session->userdata('email'));
                $this->email->to($GLOBALS['dcfg']->email);
                $this->email->subject('New Listing: '.$fields['name']);
                
                $message = "<h2>New listing for approval:</h2>";
                $message .= "<p>";
                foreach($fields as $field=>$value) {
                    $message .= "<b>".htmlspecialchars($field).":</b> ".htmlspecialchars($value)."<br>";
                }
                $message .= "</p>";
                $message .= "<p>Click to approve -> " . anchor("/manage/approve_listing/{$new_id}/".$this->_approval_hash($new_id, $fields['ref_name'])) . "</p>";
                $this->email->message($message);
                $this->email->send();
            }
            
            return $new_id;
        }
        
        log_message('error', 'Failed to create a new listing.');
        return 0;
    }    
    
    /** 
     * Searches for locations
     * 
     * @param string
     * @return array[object]
     */
    function find_locations_by_name($name) {
        $this->db->like('name', $name);
        $this->db->join('states', 'states.state_id=locations.state_id', 'left');
        $this->db->where('date_deleted is NULL', NULL, FALSE);
        $this->db->where('approved', 1);
        $this->db->where('geocode_status', '200');
        $this->db->limit(100);
        
        $query = $this->db->get('locations');
        return $query->result();
    }

    /**
     * Searches for locations by proximity
     * 
     * @param string
     * @param string
     * @param int
     * @return array[object]
     */
    function find_locations($long, $lat, $distance=50) {
        $sql = "SELECT *, 
                        (((acos(sin((".$lat."*pi()/180)) * sin((`latitude`*pi()/180))+cos((".$lat."*pi()/180)) * cos((`latitude`*pi()/180)) * cos(((".$long."- `longitude`)*pi()/180))))*180/pi())*60*1.1515*1.609344)
                    AS distance
                    FROM locations
                        LEFT JOIN states ON states.state_id=locations.state_id 
                    WHERE 
                        geocode_status='200' AND 
                        approved=1 AND 
                        date_deleted IS NULL 
                    HAVING distance<='".$distance."' 
                    ORDER BY distance ASC
                    LIMIT 200";         
        
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    /**
     * Adds 1 to views
     * 
     * @param id
     * @return void
     */
    function increment_profile_views($location_id) {
        $sql="UPDATE locations SET profile_views=profile_views+1 WHERE id=? LIMIT 1";
        
        $query = $this->db->query($sql, $location_id);
    }

    /**
     * Deletes a location
     * 
     * @param int
     * @return bool
     */
    function delete_location($id) {
        //$this->db->set('date_deleted', 'NOW()', FALSE);
        //$this->db->set('ref_name', '');
        $this->db->where('id', $id);
        $this->db->limit(1);
        
        $query = $this->db->delete('locations');
        if($this->db->affected_rows() == 1) return TRUE;

        return FALSE;
    }
    
    /**
     * Get a single location's properties by ref name
     * 
     * @param string
     * @param bool
     * @return object
     */
    function get_location_data($ref_name, $is_approved=true) {
        $this->db->join('states', 'states.state_id=locations.state_id', 'left');
        $this->db->join('countries', 'countries.country_id=states.country_id', 'left');
        $this->db->where('ref_name', $ref_name);
        $this->db->where('date_deleted is NULL', NULL, FALSE);
        if($is_approved) $this->db->where('approved', '1');
        
        $query = $this->db->get('locations');
        if($query->num_rows() == 1) {
            return $query->row();
        } elseif ($query->num_rows() > 1) {
            log_message('error', 'More then one location was found with ref_name = '.$ref_name);
        }
        return NULL;
    }
    
    /**
     * Get single location's properties by ID.
     * 
     * @param int 
     * @param bool
     * @return object
     */
    function get_location_by_id($id) {
        $this->db->join('states', 'states.state_id=locations.state_id', 'left');
        $this->db->join('countries', 'countries.country_id=states.country_id', 'left');
        $this->db->where('id', $id);
        $this->db->where('date_deleted is NULL', NULL, FALSE);

        $query = $this->db->get('locations');
        if($query->num_rows() == 1) {
            return $query->row();
        } elseif ($query->num_rows() > 1) {
            log_message('error', 'More then one location was found with id = '.$id);
        }
        return NULL;
    }   

    /** 
     * Returns all unique cities for a state
     *
     * @param int
     * @return array[object]
     */ 
    function get_cities($state_id) {                
        $this->db->select('count(id) as count, city');
        $this->db->where("city !=", '');
        $this->db->where('state_id', $state_id);
        $this->db->where('date_deleted is NULL', NULL, FALSE);
        $this->db->where('approved', 1);
        $this->db->group_by('city');
        $this->db->order_by('city', 'asc');

        $query = $this->db->get('locations');
        return $query->result();
    } 


    /** 
     * Checks to see if a ref_name is available
     *
     * @param string
     * @param string
     * @param string
     * @return bool
     */
    function ref_name_available($ref_name, $field='ref_name', $table='locations') {
        $this->db->select($field);
        $this->db->where($field, $ref_name);
        $this->db->limit(1);
        
        $query = $this->db->get($table);
        if($query->num_rows() == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /** 
     * Formats the $name as a ref_name. leaving only dashes/alpha
     *
     * @param str
     * @return str
     */
    function name_to_ref($name) {
        $ref_name = strtolower(preg_replace("@\s@", "-", $name));   // white space -> dash, lower case
        $ref_name = preg_replace("/[^a-z-]/i", '', $ref_name);      // remove all except letters and dash
        $ref_name = preg_replace("/-+/", "-", $ref_name);           // fix multiple dashes
        $ref_name = preg_replace("/^-/", "", $ref_name);            // remove pre-dashes
        $ref_name = preg_replace("/-$/", "", $ref_name);            // remove traling dashes 
        return $ref_name;
    }
    
    /**
    * Returns an available ref_name based off $name trying the following
    * formats: name, name+city, name+city+number
    * 
    * @param array (includes 'name' and 'city'
    * @param str
    * @param str
    * @return str
    */
    function make_ref_name($location, $field='ref_name', $table='locations') {
        // Try just the name
        // ie. my-location
        $ref_name = $this->name_to_ref($location['name']);
        if($this->ref_name_available($ref_name, $field, $table)) return $ref_name;

        // Try appending the city (if it isn't already there)
        // ie. my-location-vancouver
        $city_ref = $this->name_to_ref($location['city']);
        $has_city_suffix = preg_match('/'.$city_ref.'\d*$/i', $ref_name) > 0 ? true : false;
        if( !$has_city_suffix) {
            $ref_name = sprintf("%s-%s", $ref_name, $city_ref);
            if($this->ref_name_available($ref_name, $field, $table)) return $ref_name;            
        }

        // Finally, try adding a numeric suffix of 2 or incrementing the existing suffix    
        while(true) {
            $ref_name = $this->_increment_suffix($ref_name);
            if($this->ref_name_available($ref_name, $field, $table)) return $ref_name;
        }
    }

    /** 
     * Increments the number at the end of the string. If there's no number it appends a '2'.
     *
     * @param str
     * @return str
     */
    function _increment_suffix($string) {
        $has_number_suffix = preg_match('/(\d+)$/i', $string, $match);

        if($has_number_suffix == 0) {
            return $string . '2';

        } else {
            $number = (int)$match[1];
            return preg_replace('/\d+$/i', '', $string) . ($number + 1);
        }
    }

}

?>
